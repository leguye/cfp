import { Product, ProductsRepository } from '../../src/products';

describe('List all products', () => {
  it('should return an empty array', () => {
    const productsRepository = new ProductsRepository();
    expect(productsRepository.all).toEqual([]);
  });
  it('should return all products', () => {
    const PRODUCTS: Product[] = [{ id: 'p1', cfp: 10 }];
    const productsRepository = new ProductsRepository(PRODUCTS);
    expect(productsRepository.all).toEqual(PRODUCTS);
  });
});
