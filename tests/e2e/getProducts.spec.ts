import request from 'supertest';
import {service} from '../../src/index';
import { ProductsRepository } from '../../src/products';

describe('GET /products', () => {
  it('should send a response with all the products in its body', async () => {
    const spy =  jest.spyOn(ProductsRepository.prototype, 'all', 'get');
    await request(service).get('/products')
      .expect(200)
      .expect('content-type', /json/);
    expect(spy).toHaveBeenCalled();
  });
});
