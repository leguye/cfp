export default {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: 'coverage',
  coverageProvider: 'v8',
  transform: {
    '\\.(ts)$': 'ts-jest',
  },
  coverageReporters: ['html', 'text', 'text-summary'],
  watchPathIgnorePatterns: ['\\.(json)$'],
};
