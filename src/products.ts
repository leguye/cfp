export interface Product {
  id: string;
  cfp: number;
}

export class ProductsRepository {
  private products: Product[];
  
  constructor(products: Product[] = []) {
    this.products = products;
  } 
  
  get all() {
    return this.products;
  }
}
