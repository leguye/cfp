import express from 'express';
import { ProductsRepository, Product } from './products';
import fs from 'fs';

export const service = express();

service.get('/products', (request, response) => {
  const products: Product[] = JSON.parse(fs.readFileSync('./data/products.json', 'utf-8'));
  const productsRepository = new ProductsRepository(products);
  response.status(200).json(productsRepository.all);
});

service.listen(8888);